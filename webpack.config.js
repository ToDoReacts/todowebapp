const webpack = require("webpack");
const path = require("path");
const envFile = require("node-env-file");
let assert = require("assert");

// რა ეტაპზეა აპლიკაცია (production, development...)
// ხელით გადართულია პროდაქშენში, სერვერზე არ გამოდგება სავარაუდოდ
process.env.NODE_ENV = process.env.NODE_ENV || "development";

// process.env.NODE_ENV = 'production';

// process env-იმ რომ დაინახოს ჩვენი კონფიგ ფაილები
try {
  envFile(path.join(__dirname, "config/" + process.env.NODE_ENV + ".env"));
} catch (error) {
  console.log(error);
}

module.exports = {
  // საწყისი ფაილი
  entry: [
    // script!-ით ვიძახებთ script loaders და ვუთითებთ რო webpack-ით არ გადავუაროთ და უბალოდ ჩავტვირთოთ ფაილი
    "script!jquery/dist/jquery.min.js",
    "script!foundation-sites/dist/foundation.min.js",
    "./app/app.jsx"
  ],
  externals: {
    jquery: "jQuery"
  },
  plugins: [
    // foundation-მა რომ იცნოს  jQuery
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        API_KEY: JSON.stringify(process.env.API_KEY),
        AUTH_DOMAIN: JSON.stringify(process.env.AUTH_DOMAIN),
        DATABASE_URL: JSON.stringify(process.env.DATABASE_URL),
        PROJECT_ID: JSON.stringify(process.env.PROJECT_ID),
        STORAGE_BUCKET: JSON.stringify(process.env.STORAGE_BUCKET),
        MESSAGING_SENDER_ID: JSON.stringify(process.env.MESSAGING_SENDER_ID)
      }
    })
  ],
  // ფაილი რომელსაც დააგენერირებს
  output: {
    path: __dirname,
    filename: "./public/bundle.js"
  },
  resolve: {
    //რომელი ფოლდერიდან დაიწყოს ფაილების მოძებნა - ამ შემთხვევაში პროექტის ფოლდერია
    root: __dirname,
    // ყველა ფაილის წაკითხვა რაც ამ ფოლდერშია და მიმთითებლის გაკეთება
    modulesDirectories: ["node_modules", "./app/components", "./app/api"],
    //ფაილების მისამართების გაწერა და მიმთითებლის შექმნა
    alias: {
      app: "app",
      style: "app/style/style.scss",
      actions: "app/actions/actions.jsx",
      reducers: "app/reducers/reducers.jsx",
      store: "app/store/configureStore.jsx"
    },
    // ფაილის გაფართოებები რომელსაც შეეხება
    extensions: ["", ".js", ".jsx"]
  },
  module: {
    // ბიბლიოთეკა რომლითაც წაიკითხავს ფაილებს
    loaders: [
      {
        // ბაბელის წამკითხველი
        loader: "babel-loader",
        // რეაქტის და es6 ის წაკითხვა და კონვერტირება რომ შეძლოს
        query: {
          presets: ["react", "es2015", "stage-0"]
        },
        // გაფართოება
        test: /\.jsx?$/,
        // რომელ ფოლდერში არ შეეხოს ფაილებს
        exclude: /(node_modules | bower_components)/,
        exclude: /node_modules/
      }
    ]
  },
  // საიდან წაიკითხოს sass ფაილები
  sassLoader: {
    includePaths: [
      path.resolve(__dirname, "./node_modules/foundation-sites/scss")
    ]
  },
  // ბანდლის ისე წაკითხვა კონსოლში როგორც სამუშაო ფაილშია (დებაგერთან ერთად?!)
  devtool:
    process.env.NODE_ENV === "production"
      ? undefined
      : "cheap-module-eval-source-map"
};
