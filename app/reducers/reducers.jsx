const uuid = require('node-uuid');
const moment = require('moment');

export let searchToDoReducer = (state = '', action) => {
    switch (action.type) {
        case 'SET_SEARCH_TEXT':
            return state = action.searchText;
        default:
            return state;
    }
}

export let showCompletedReducer = (state = false, action) => {
    switch(action.type) {
        case 'TOGGLE_SHOW_COMPLETED':
            return !state;       
        default:
            return state;
    }
}

export let toDoReducer = (state = [], action) => {
    switch(action.type) {
        case 'ADD_TODO': 
            return (
                [
                    ...state,
                    action.toDo
                ]
            );
        case 'UPDATE_TODO': 
            return state.map((todo) => { 
                if(todo.id === action.id){
                    return {
                        ...todo,
                        ...action.updates
                    }
                }
                    return todo;
            });
        case 'LOGOUT':
            return [];
        case 'ADD_TODOS':
            return [
                ...state,
                ...action.toDos
            ]
        default: {
            return state
        }
    }
}

export let authReducer = (state = {}, action) => {
    switch(action.type) {
        case 'LOGIN':
            return {
                ...state,
                uid: action.uid,
                user: action.displayName,
                photoURL: action.photoURL
            }
        case 'LOGOUT':
            return {}
        default: {
            return state
        }
    }
}