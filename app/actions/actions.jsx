import firebase, {firebaseRef, githubProvider} from 'app/firebase/index';
import moment from 'moment';

export let setSearchText = (searchText) => {
    return{
        type: 'SET_SEARCH_TEXT',
        searchText
    }
}

export let addToDos = (toDos) => {
    return {
        type: 'ADD_TODOS',
        toDos
    }
}

export let addToDo = (toDo) => {
    return {
        type: 'ADD_TODO',
        toDo
    }
}

export let startAddToDos = (text) => {
    return (dispatch, getState) => {
        {
            let toDo = {
                text,
                checked: false,
                createdTimestamp: moment().unix(),
                completedTimestamp: null
            }
            let uid = getState().auth.uid;
            let toDoRef = firebaseRef.child(`users/${uid}/toDos`).push(toDo);

            return toDoRef.then(() => {
                dispatch(addToDo({
                    ...toDo,
                    id: toDoRef.key
                }))
            });
        }
    }
}

export let startAddToDosInView = () => {
    return (dispatch, getState) => {
        let uid = getState().auth.uid;
        let toDosRef = firebaseRef.child(`users/${uid}/toDos`);
        let toDos = [];
        return toDosRef.once('value').then((snapshot) => {
            var toDoVal = snapshot.val();
            var toDoKey = snapshot.key;     
            Object.keys(toDoVal).forEach((toDoId) => {
                toDos.push({
                    id: toDoId,
                    ...toDoVal[toDoId]
                })
            });
            dispatch(addToDos(toDos));
        });
    }
}

export let toggleShowCompleted = () => {
    return {
        type: 'TOGGLE_SHOW_COMPLETED'
    }
}

export let updateToDo = (id, updates) => {
    return {
        type: 'UPDATE_TODO',
        id,
        updates
    }
}

export let startToggleToDo = (id, checked) => {
    return (dispatch, getState) => {
        let uid = getState().auth.uid;
        let toDoRef = firebaseRef.child(`users/${uid}/toDos/${id}`);
        let updates = {
            checked,
            completedTimestamp: checked ? moment().unix() : null
        }

        return toDoRef.update(updates).then(() => {
            dispatch(updateToDo(id, updates));
        });
    }
}

export let startLogin = () => {
    return (dispatch, getState) => {
        return firebase.auth().signInWithPopup(githubProvider).then((result) => {
            //console.log('Loged In:', result);
        }, (error) => {
            //console.log('Unable To Login:', error);
        })
    }
}

export let startLogout = () => {
    return (dispatch, getState) => {
        return firebase.auth().signOut().then((result) => {
            //console.log('Loged out!');
        }, (error) => {
            //console.log('Unable To Logout:', error);
        })
    }
}

export let login = (uid,displayName,photoURL) => {
    return {
        type: "LOGIN",
        uid,
        displayName,
        photoURL
    }
}

export let logout = () => {
    return {
        type: 'LOGOUT'
    }
}