import React from "react";
import { connect } from "react-redux";

// my comp
import ToDo from "ToDo";
import ManageToDos from "ManageToDos";

export class ToDoList extends React.Component {
  render() {
    let { toDos, searchText, showCompleted } = this.props;

    let renderTodo = () => {
      let filteredToDos = ManageToDos.filterToDos(
        toDos,
        showCompleted,
        searchText
      );

      if (filteredToDos.length === 0) {
        return (
          // კლასის შეცლისას ლისტის ტესტშიც შეიტანე ცვლილება
          <p className="message_empty-todos">Nothing to do!</p>
        );
      }
      return filteredToDos.map(toDo => {
        return (
          <div>
            <ToDo key={toDo.id} {...toDo} />
            <div className="divider-line" />
          </div>
        );
      });
    };

    return <div id="todo-list">{renderTodo()}</div>;
  }
}

export default connect(state => {
  return state;
})(ToDoList);
