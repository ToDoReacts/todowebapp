import React, { createElement } from "react";
import { connect } from "react-redux";
const actions = require("actions");

export class ToDoSearch extends React.Component {
  render() {
    let { dispatch, searchText } = this.props;

    return (
      <div id="search">
        <img
          src={`./img/icons/search.svg`}
          className="search-icon"
          alt="search"
        />
        <input
          id="search-input"
          type="search"
          placeholder="search"
          ref="searchText"
          value={searchText}
          onChange={() => {
            let searchText = this.refs.searchText.value;
            dispatch(actions.setSearchText(searchText));
          }}
        />
      </div>
    );
  }
}

export default connect(state => {
  return {
    searchText: state.searchText
  };
})(ToDoSearch);
