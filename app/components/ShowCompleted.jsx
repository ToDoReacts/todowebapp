import React from "react";
import { connect } from "react-redux";
const actions = require("actions");
import $ from "jquery";

export class ShowCompleted extends React.Component {
  changBTN() {
    let btn = document.getElementById("show-completed");
    $(btn).toggleClass("active-btn");
  }
  render() {
    let { dispatch, showCompleted } = this.props;

    return (
      <label id="show-completed">
        <input
          type="checkbox"
          ref="showCompleted"
          value={showCompleted}
          onChange={() => {
            dispatch(actions.toggleShowCompleted());
            this.changBTN();
          }}
        />
        <p className="button-text">Show Completed</p>
      </label>
    );
  }
}

export default connect(state => {
  return {
    showCompleted: state.showCompleted
  };
})(ShowCompleted);
