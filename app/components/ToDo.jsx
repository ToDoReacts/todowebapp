import React from "react";
import { connect } from "react-redux";
import moment from "moment";

// my comp
import * as actions from "actions";

export class ToDo extends React.Component {
  render() {
    let {
      text,
      id,
      checked,
      createdTimestamp,
      completedTimestamp,
      dispatch
    } = this.props;

    let renderDate = () => {
      let nowTimestamp, status;

      if (checked) {
        nowTimestamp = completedTimestamp;
        status = "Completed at";
      } else {
        nowTimestamp = createdTimestamp;
        status = "Created at";
      }
      let formatedTime = moment.unix(nowTimestamp).format(`hh:mm A | D.MM.YY`);
      let message = `${status}: ${formatedTime}`;
      return message;
    };
    let checkboxStyle = () => {
      if (checked) {
        return { backgroundImage: `url('img/icons/checkbox-archive.svg')` };
      } else {
        return { backgroundImage: `url('img/icons/checkbox.svg')` };
      }
    };
    let textStyle = () => {
      if (checked) {
        return { textDecoration: "line-through" };
      }
    };
    return (
      <div
        className="todo"
        onClick={() => {
          dispatch(actions.startToggleToDo(id, !checked));
        }}
      >
        <input type="checkbox" checked={checked} />
        <span className="checkbox-style" style={checkboxStyle()} />
        <p className="todo-text" style={textStyle()}>
          {text}
        </p>
        {/*როდის შეიქმნა todo? => <br/>
                {`${renderDate()}`} */}
      </div>
    );
  }
}

export default connect()(ToDo);
