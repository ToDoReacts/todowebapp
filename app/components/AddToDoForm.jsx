import React from "react";
import { connect } from "react-redux";
import * as actions from "actions";

export class AddToDoForm extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.submitForm = this.submitForm.bind(this);
  }

  submitForm(e) {
    let { dispatch } = this.props;
    e.preventDefault();
    let toDo = this.refs.toDo.value;
    if (toDo.length > 0) {
      this.refs.toDo.value = "";
      dispatch(actions.startAddToDos(toDo));
    } else {
      this.refs.toDo.focus();
    }
  }

  render() {
    return (
      <div id="add-todo-form">
        <img src="./img/icons/plus.svg" className="plus-icon" alt="add todo" />
        <form onSubmit={this.submitForm}>
          <input type="text" ref="toDo" placeholder="add todo" />
        </form>
      </div>
    );
  }
}

export default connect()(AddToDoForm);
