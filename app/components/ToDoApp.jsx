import React from "react";
import * as Redux from "react-redux";

// my comps
import Header from "Header";
import ToDoList from "ToDoList";
import AddToDoForm from "AddToDoForm";
import * as actions from "actions";
import ShowCompleted from "ShowCompleted";

export class ToDoApp extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <div className="main-container">
          <AddToDoForm />
          <div className="all-todos">
            <ToDoList />
            <ShowCompleted />
          </div>
        </div>
      </div>
    );
  }
}

export default Redux.connect()(ToDoApp);
