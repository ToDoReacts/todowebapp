const React = require("react");
import { connect } from "react-redux";

//my comp
import * as actions from "actions";

class Index extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onLogin = this.onLogin.bind(this);
  }

  onLogin() {
    let { dispatch } = this.props;
    dispatch(actions.startLogin());
  }
  render() {
    return (
      <div id="index">
        <div className="log-in-window">
          <img className="red-logo" src="img/logo-red.svg" alt="Ptichka" />
          <h4>Log In With GitHub</h4>
          <p>& Do More.</p>
          <button className="github-btn" onClick={this.onLogin}>
            <img className="github" src="img/github.svg" alt="GitHub" />
            Log In with GitHub
          </button>
        </div>
      </div>
    );
  }
}

export default connect()(Index);
