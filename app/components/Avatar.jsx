import React from "react";
import { connect } from "react-redux";
import $ from "jquery";

// my comp
import * as actions from "actions";

export class Avatar extends React.Component {
  constructor(props, context) {
    super(props, context);
    
    this.showUserDropDown = this.showUserDropDown.bind(this);
    this.onLogout = this.onLogout.bind(this);
  }

  onLogout(event) {
    let { dispatch } = this.props;
    event.preventDefault();
    dispatch(actions.startLogout());
  }

  showUserDropDown(event) {
    let userDropDown = document.getElementsByClassName("user-drop-down")[0];
    $(userDropDown).toggleClass("hide-user-dorp-down");
  }

  render() {
    let { auth } = this.props;
    let userName = auth.user;
    let profilePicture = auth.photoURL;

    let avatarStyle = {
      backgroundImage: "url(" + profilePicture + ")"
    };
    return (
      <div id="active-user-avatar">
        <div
          className="avatar"
          onClick={this.showUserDropDown}
          style={avatarStyle}
          alt="avatar"
        />

        <nav className="user-drop-down hide-user-dorp-down">
          <div className="triangle" />
          <header>
            <div className="avatar" style={avatarStyle} alt="avatar" />
            <h3>{userName}</h3>
          </header>
          <div className="divider-line" />
          <ul>
            <footer>
              <li>
                <a onClick={this.onLogout}>Logout</a>
              </li>
            </footer>
          </ul>
        </nav>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    auth: state.auth
  };
}

export default connect(mapStateToProps)(Avatar);
