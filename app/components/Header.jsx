import React from "react";

//my comp
import ToDoSearch from "ToDoSearch";
import Avatar from "Avatar";

const Header = () => {
  return (
    <header id="main-header">
      <div className="container">
        <img id="logo" src="./img/logo.svg" alt="Ptichka" />
        <div className="right-side">
          <ToDoSearch />
          <Avatar />
        </div>
      </div>
    </header>
  );
};

export default Header;
