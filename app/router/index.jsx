import React from 'react';
import {Route, Router, IndexRoute, hashHistory} from 'react-router';

//my comp
import firebase from 'app/firebase/';
import ToDoApp from 'ToDoApp';
import Index from 'Index';


let requireLogin = (nextState, replace, next) => {
    if(!firebase.auth().currentUser){
        replace('/');
    }
    next();
}

let redirectIfLoggedIn = (nextState, replace, next) => {
    if(firebase.auth().currentUser){
        replace('/todos');
    }
    next();
}

export default (
    <Router history={hashHistory}>
        <Route path="/"> 
            <IndexRoute component={Index} onEnter={redirectIfLoggedIn}/>
            <Route path="/todos" component={ToDoApp} onEnter={requireLogin}/>
        </Route>
    </Router>
)