import * as redux from 'redux';
import {searchToDoReducer, showCompletedReducer, toDoReducer, authReducer} from 'reducers';
import thunk from 'redux-thunk';

export let configure = (initialState) => {

    let reducer = redux.combineReducers({
        searchText: searchToDoReducer,
        showCompleted: showCompletedReducer,
        toDos: toDoReducer,
        auth: authReducer
    })

    let store = redux.createStore(reducer, initialState, redux.compose(
        redux.applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    ));

    return store;
};