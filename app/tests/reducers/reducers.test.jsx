const expect = require('expect');
// შემოწმება შეიცვალა თუ არა რედუსერის გამოძახებით გადაწოდებული პარამეტრი, თუ კი ტესტი დაფეილდება
const dfs = require('deep-freeze-strict');

// my comp
const reducers = require('reducers');

describe('Reducers', () => {

    describe('setSearchReducer', () => {
        it('should set search text', () => {
            let action = {
                type: 'SET_SEARCH_TEXT',
                searchText: 'some txt'
            }
            let result = reducers.searchToDoReducer(dfs(''), dfs(action));
            expect(result).toEqual(action.searchText);
        });
    });

    describe('toggleShowCompletedReducer', () => {
        it('should set toggle show completed - from true to false', () => {
            let action = {
                type: 'TOGGLE_SHOW_COMPLETED'
            }
            let result = reducers.showCompletedReducer(dfs(true), dfs(action));
            expect(result).toBe(false);
        });

        it('should set toggle show completed - from false to true', () => {
            let action = {
                type: 'TOGGLE_SHOW_COMPLETED'
            }
            let result = reducers.showCompletedReducer(dfs(false), dfs(action));
            expect(result).toBe(true);
        });
    });

    describe('toDoReducer', () => {
        it('should render existing todos', () => {
            let todos = [{
                id: '454',
                text: 'action.text',
                checked: false,
                createdTimestamp: 45478,
                completedTimestamp: undefined
            }];
            let action = {
                type: 'ADD_TODOS',
                toDos: todos
            }
            let result = reducers.toDoReducer(dfs([]), dfs(action));

            expect(result.length).toEqual(1);
            expect(result[0]).toEqual(todos[0]);
        });


        it('should add new todo', () => {
            let action = {
                type: 'ADD_TODO',
                toDo: {
                    text: 'some todo',
                    checked: false,
                    createdTimestamp: 7878878454,
                    completedTimestamp: null
                }
            }
            let result = reducers.toDoReducer(dfs([]), dfs(action));

            expect(result.length).toBe(1);
            expect(result[0]).toEqual(action.toDo);
        });

        it('should update todos', () => {
            let sampleTodo = [{
                type: 'ADD_TODO',
                id: '123',
                text: 'sample todo',
                checked: true,
                createdTimestamp: 157,
                completedTimestamp: 195
            }];
            let updates = {
                checked: false,
                completedTimestamp: null
            }
            let action = {
                type: 'UPDATE_TODO',
                id: sampleTodo[0].id,
                updates
            }

            let result = reducers.toDoReducer(dfs(sampleTodo), dfs(action));

            expect(result[0].checked).toEqual(updates.checked);
            expect(result[0].completedTimestamp).toEqual(updates.completedTimestamp);
            expect(result[0].text).toEqual(sampleTodo[0].text);
        });

        it('should delete locally todos on logout', () => {
            let sampleTodo = [{
                type: 'ADD_TODO',
                id: '123',
                text: 'sample todo',
                checked: true,
                createdTimestamp: 157,
                completedTimestamp: 195
            }];
            let action = {
                type: 'LOGOUT'
            }
            let result = reducers.toDoReducer(dfs(sampleTodo), dfs(action));
            expect(result.length).toEqual(0);
        });
    });

    describe('authReducer', () => {
        it('should call LOGIN action and add uid in store', () => {
            let action = {
                type: 'LOGIN',
                uid: 'dsdadksadka'
            };
            let result = reducers.authReducer({},dfs(action));

            expect(result.uid).toEqual(action.uid);
        });
        it('should call LOGOUT action and delete auth value', () => {
            let authData = {
                uid: '454'
            };
            let action = {
                type: 'LOGOUT'
            };
            let result = reducers.authReducer(dfs(authData),dfs(action));

            expect(result).toEqual({});
        });
    });

});