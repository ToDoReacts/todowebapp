const React = require('react');
const ReactDOM = require('react-dom');
const {Provider} = require('react-redux');
const TestUtil = require('react-addons-test-utils');
const expect = require('expect');
const $ = require('jquery');

// my comp
import ConnectedToDoList, {ToDoList} from 'ToDoList';
import ConnectedToDo, {ToDo} from 'ToDo';
import {configure} from 'store';

describe(
    'ToDoList',
    () => {
        it(
            'Should Exist',
            () => {
                expect(ToDoList).toExist(); 
            }
        );
        
        it(
            'should render one todo in list on one todo',
            () => {
                let toDos = [
                    {
                        id: '1',
                        text: 'make some',
                        checked: false,
                        createdTimestamp: 757,
                        completedTimestamp: undefined
                    },
                    {
                        id: '787',
                        text: 'make something else',
                        checked: false,
                        createdTimestamp: 757,
                        completedTimestamp: undefined
                    }
                ];
                let store = configure({
                    toDos: toDos
                });
                let provider = TestUtil.renderIntoDocument(
                <Provider store={store}>
                    <ConnectedToDoList/>
                </Provider>);

                let todoList = TestUtil.scryRenderedComponentsWithType(provider, ConnectedToDoList)[0];
                let toDoArr = TestUtil.scryRenderedComponentsWithType(todoList, ConnectedToDo);

                expect(toDoArr.length).toBe(toDos.length);
            }
        );

        it(
            'should return message if todos array is empty',
            () => {
                let toDos = [];
                let store = configure({
                    toDos: toDos
                });
                let provider = TestUtil.renderIntoDocument(
                <Provider store={store}>
                    <ConnectedToDoList/>
                </Provider>);

                let todoList = TestUtil.scryRenderedComponentsWithType(provider, ConnectedToDoList)[0];
                let toDoArr = TestUtil.scryRenderedComponentsWithType(todoList, ConnectedToDo);
                let $el = $(ReactDOM.findDOMNode(provider));

                expect($el.find('.message_empty-todos').length).toBe(1);
            }
        );

    }
)