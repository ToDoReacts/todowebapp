const React = require('react');
const ReactDOM = require('react-dom');
const TestUtil = require('react-addons-test-utils');
const expect = require('expect');
const $ = require('jquery');

// my comp
import {ToDoSearch} from 'ToDoSearch';

describe(
    'ToDoSearch',
    () => {

        it(
            'should exist',
            () => {
                expect(ToDoSearch).toExist();
            }
        );
        
        it(
            'should call SET_SEARCH_TEXT action on input change',
            () => {
                let text = 'Some';
                let action = {
                    type: 'SET_SEARCH_TEXT',
                    searchText: text
                }
                let spy = expect.createSpy();
                let toDoSearch = TestUtil.renderIntoDocument(<ToDoSearch dispatch={spy}/>);

                toDoSearch.refs.searchText.value = text;
                TestUtil.Simulate.change(toDoSearch.refs.searchText);

                expect(spy).toHaveBeenCalledWith(action);
            }
        );
    }
)