const React = require('react');
const ReactDOM = require('react-dom');
const TestUtil = require('react-addons-test-utils');
const expect = require('expect');
const $ = require('jquery');

// my comp
const {AddToDoForm} = require('AddToDoForm');
import * as actions from 'actions';

describe(
    'AddToDoForm',
    () => {
        it(
            'should exist',
            () => {
                expect(AddToDoForm).toExist();
            }
        ),
        it(
            'should not to be called function if input length is = 0',
            () => {
                let spy = expect.createSpy();
                let addToDoForm = TestUtil.renderIntoDocument(<AddToDoForm dispatch={spy}/>);
                let $el = $(ReactDOM.findDOMNode(addToDoForm));

                addToDoForm.refs.toDo.value = '';
                TestUtil.Simulate.submit($el.find('form')[0]);

                expect(spy).toNotHaveBeenCalled();
            }
        ),
        it(
            'should dispatch ADD_TODO if valid data is entered',
            () => {
                let text = 'some todo';
                let action = actions.startAddToDos(text);
                let spy = expect.createSpy();
                let addToDoForm = TestUtil.renderIntoDocument(<AddToDoForm dispatch={spy}/>);
                let $el = $(ReactDOM.findDOMNode(addToDoForm));

                addToDoForm.refs.toDo.value = text;
                TestUtil.Simulate.submit($el.find('form')[0]);

                expect(spy).toHaveBeenCalledWith(action);
            }
        )
    }
)