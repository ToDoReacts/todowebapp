const React = require('react');
const ReactDOM = require('react-dom');
const TestUtil = require('react-addons-test-utils');
const expect = require('expect');
const $ = require('jquery');

// my comp
const {ToDo} = require('ToDo');
import * as actions from 'actions';

describe(
    'ToDo',
    () => {
        it('Should Exist', () => {
                expect(ToDo).toExist(); 
        });

        it('should dispatch TOGGLE_TODO action on click', () => {
            let toDoData = {
                id: "f1ee166f-a631-4d33-9573-121689f4b445",
                text: 'some todo',
                checked: true
            };

            let action = actions.startToggleToDo(toDoData.id, !toDoData.checked);

            let spy = expect.createSpy();
            let toDo = TestUtil.renderIntoDocument(<ToDo {...toDoData} dispatch={spy} />);
            let $el = $(ReactDOM.findDOMNode(toDo));

            TestUtil.Simulate.click($el[0]);
            expect(spy).toHaveBeenCalledWith(action);
        });

    }
)