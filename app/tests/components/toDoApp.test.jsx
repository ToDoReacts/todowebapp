const React = require("react");
const ReactDOM = require("react-dom");
const { Provider } = require("react-redux");
const TestUtil = require("react-addons-test-utils");
const expect = require("expect");
const $ = require("jquery");

// my comp
import { ToDoApp } from "ToDoApp";
import ToDoList from "ToDoList";
const Store = require("store");

describe("ToDoApp", () => {
  it("Should Exist", () => {
    expect(ToDoApp).toExist();
  });

  it("should render ToDoList into document", () => {
    let store = Store.configure();
    let provider = TestUtil.renderIntoDocument(
      <Provider store={store}>
        <ToDoApp />
      </Provider>
    );

    let todoApp = TestUtil.scryRenderedComponentsWithType(provider, ToDoApp)[0];
    let todoList = TestUtil.scryRenderedComponentsWithType(todoApp, ToDoList);

    expect(todoList.length).toEqual(1);
  });
});
