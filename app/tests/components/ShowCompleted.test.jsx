const React = require('react');
const ReactDOM = require('react-dom');
const TestUtil = require('react-addons-test-utils');
const expect = require('expect');
const $ = require('jquery');

// my comp
import {ShowCompleted} from 'ShowCompleted';

describe(
    'ShowCompleted',
    () => {

        it(
            'should exist',
            () => {
                expect(ShowCompleted).toExist();
            }
        );

        it(
            'should dispatch TOGGLE_SHOW_COMPLETED action when checkbox clicked',
            () => {
                let action = {
                    type: 'TOGGLE_SHOW_COMPLETED'
                };
                let spy = expect.createSpy();
                let showCompleted = TestUtil.renderIntoDocument(<ShowCompleted dispatch={spy}/>);
                
                showCompleted.refs.showCompleted.checked = true;
                TestUtil.Simulate.change(showCompleted.refs.showCompleted);

                expect(spy).toHaveBeenCalledWith(action);
            }
        );

    }
)