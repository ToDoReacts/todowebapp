import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

// my comp
const actions = require('actions');

import firebase, {firebaseRef} from 'app/firebase/';

let createMochStore = configureMockStore([thunk]);

describe('Actions', () => {

        it('should run setSearchText action and get text', () => {
            let action = {
                type: 'SET_SEARCH_TEXT',
                searchText: 'Some Txt'
            }
            let result = actions.setSearchText(action.searchText);
            expect(result).toEqual(action);
        });

        it('should generate add todos action', () => {
            let todos = [{
                id: '454',
                text: 'action.text',
                checked: false,
                createdTimestamp: 45478,
                completedTimestamp: undefined
            }];
            let action = {
                type: 'ADD_TODOS',
                toDos: todos
            }
            let result = actions.addToDos(todos);
            expect(result).toEqual(action);
        });

        it('should run addToDo action and get todo text', () => {
            let action = {
                type: 'ADD_TODO',
                toDo: {
                    text: 'some',
                    checked: false,
                    createdTimestamp: 78,
                    completedTimestamp: null    
                }
            }
            let result = actions.addToDo(action.toDo);
            expect(result).toEqual(action);
        });

        it('should run toggleShowCompleted action', () => {
            let action = {
                type: 'TOGGLE_SHOW_COMPLETED'
            }
            let result = actions.toggleShowCompleted();
            expect(result).toEqual(action);
        });

        it('should run update todo action and get id', () => {
            let action = {
                type: 'UPDATE_TODO',
                id: '444',
                updates: {
                    checked: false
                }
            }
            let result = actions.updateToDo(action.id, action.updates);
            expect(result).toEqual(action);
        });

        describe('Test with firebase todos', () => {
            let testToDoRef;
            let uid;
            let todosRef;

            beforeEach((done) => {
                firebase.auth().signInAnonymously().then((user)=>{
                    uid = user.uid;
                    todosRef = firebaseRef.child(`users/${uid}/toDos`);

                    return todosRef.remove();
                }).then(() => {
                    testToDoRef = todosRef.push();
                    return testToDoRef.set({
                        text: 'some -------',
                        checked: false,
                        createdTimestamp: 454545
                    })
                }).then(() => done())
                .catch(done);               
            });

            afterEach((done) => {
                todosRef.remove().then(() => done());
            });

            it('should toggle todo and dispatch UPDATE_TODO action', (done) => {
                const store = createMochStore({auth:{uid}});  
                const action = actions.startToggleToDo(testToDoRef.key, true);

                store.dispatch(action).then(() => {
                    const mockActions = store.getActions();

                    expect(mockActions[0]).toInclude({
                        type: 'UPDATE_TODO',
                        id: testToDoRef.key
                    });

                    expect(mockActions[0].updates).toInclude({
                        checked: true
                    });

                    expect(mockActions[0].updates.completedTimestamp).toExist();

                    done();
                }, done);
            });

            it('should startAddToDosInView call with valid data and dispatch ADD_TODOS', (done) =>{
                const store = createMochStore({auth:{uid}});   
                const action = actions.startAddToDosInView();

                store.dispatch(action).then(() => {
                    const mockActions = store.getActions();

                    expect(mockActions[0].type).toInclude('ADD_TODO');
                    expect(mockActions[0].toDos.length).toEqual(1);
                    expect(mockActions[0].toDos[0].text).toEqual('some -------');

                    done();
                }, done)
            });

            it('should add new todo and dispatch ADD_TODO', (done) => {
                const store = createMochStore({auth:{uid}}); 
                const text = 'some todo---';
    
    
                // needed remove todo after runing test
                store.dispatch(actions.startAddToDos(text)).then(() => {
                    const actions = store.getActions();
                    expect(actions[0]).toInclude({type: 'ADD_TODO'});
                    expect(actions[0].toDo).toInclude({text: text});
                    done();
                }).catch(done);
            });
        });

        describe('Login & Logout', () => {
            it('should generate login obj', () =>{
                let action = {
                    type: 'LOGIN',
                    uid: '4545454',
                    displayName: 'sano',
                    photoURL: 'url'
                }
                let result = actions.login(action.uid,action.displayName, action.photoURL);
                expect(action).toEqual(result);
            });
            it('should generate logout obj', () => {
                let action = {
                    type: 'LOGOUT',
                }
                let result = actions.logout();
                expect(action).toEqual(result);
            });
        });



    }
);