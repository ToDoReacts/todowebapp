const expect = require('expect');

// my comp
const ManageToDos = require('ManageToDos');

describe(
    'ManageToDos',
    () => {
        // ყოველი ტესტის შესრულებამდე რა შეასრულოს
        beforeEach(() => {
            localStorage.removeItem('todos');
        });

        it(
            'should exist',
            () => {
                expect(ManageToDos).toExist();
            }
        );

        describe(
            'setTodo',
            () => {

                it(
                    'should set correct value to local storage',
                    () => {
                        let todos = [{
                            id: 4,
                            text: 'some task',
                            checked: false
                        }];
        
                        ManageToDos.setToDos(todos);
                        let setedTodo = JSON.parse(localStorage.getItem('todos'));
                        expect(todos).toEqual(setedTodo);
                    }
                );

                it(
                    'should don`t call function with bad data',
                    () => {
                        let badTodos = {text: 'bad to do'};
        
                        ManageToDos.setToDos(badTodos);
                        expect(localStorage.getItem('todos')).toBe(null);
                    }
                );

            }
        );

        describe(
            'getTodo',
            () => {

                it(
                    'should get correct value from local storage, on valid data',
                    () => {
                        let todos = [{
                            id: 4,
                            text: 'some task',
                            checked: false
                        }];
        
                        localStorage.setItem('todos', JSON.stringify(todos));                      
                        let setedTodo = ManageToDos.getToDos();
                        expect(todos).toEqual(setedTodo);
                    }
                );

                it(
                    'should return empty array on invalid data',
                    () => {
                        let badTodos = {text: 'bad to do'};
        
                        localStorage.setItem('todos', JSON.stringify(badTodos));                      
                        let setedTodo = ManageToDos.getToDos();
                        expect(setedTodo).toEqual([]);
                    }
                );

            }
        );

        describe(
            'filterToDo',
            () => {

                let todos = [{
                    id: 1,
                    text: 'some task',
                    checked: false
                },
                {
                    id: 2,
                    text: 'some task',
                    checked: true
                },
                {
                    id: 3,
                    text: 'some task',
                    checked: false
                }];

                it(
                    'should return all todos if show completed is true - completed and non completed',
                    () => {
                        let filteredToDos = ManageToDos.filterToDos(todos, true, '');
                        expect(filteredToDos.length).toBe(3);
                    }
                );

                it(
                    'should return only incompleted todos if show completed is false',
                    () => {
                        let filteredToDos = ManageToDos.filterToDos(todos, false, '');
                        expect(filteredToDos.length).toBe(2);
                    }
                );

                it(
                    'should sort todos - checked first',
                    () => {
                        let filteredToDos = ManageToDos.filterToDos(todos, true, '');
                        expect(filteredToDos[2].checked).toBe(true);
                    }
                );

            }
        );

        describe(
            'searchTodo',
            () => {

                let todos = [{
                    id: 1,
                    text: 'some task',
                    checked: false
                },
                {
                    id: 2,
                    text: 'testSearchTxt',
                    checked: true
                },
                {
                    id: 3,
                    text: 'testSearchTxt sss',
                    checked: false
                }];

                it(
                    'should return all todo list on empty search input',
                    () => {
                        let filteredToDos = ManageToDos.filterToDos(todos, true, '');
                        expect(filteredToDos.length).toBe(3);
                    }
                );

                it(
                    'should return searched todos only',
                    () => {
                        let filteredToDos = ManageToDos.filterToDos(todos, true, 'testSearchTxt');
                        expect(filteredToDos.length).toBe(2);
                    }
                );

            }
        );

    }
)