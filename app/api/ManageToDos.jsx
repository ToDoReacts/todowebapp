const $ = require('jquery');

module.exports = {
    getToDos: function(){
        let strTodos = localStorage.getItem('todos');
        let todos = [];
        
        try {
            todos = JSON.parse(strTodos);
        } catch (error) {
        }

        return $.isArray(todos) ? todos : [];
    },
    setToDos: function(todos){
        if($.isArray(todos)){
            localStorage.setItem('todos', JSON.stringify(todos));
            return todos;
        }
    },
    filterToDos: function(todos, showCompleted, searchText){
        let filteredToDos = todos;

        // filter by show completed
        filteredToDos = filteredToDos.filter((todo) => {
            return showCompleted || !todo.checked;
            // შეიძლება ასე უფრო სწორი იყოს: return !todo.checked || showCompleted;
        });

        // filter by search text
        filteredToDos = filteredToDos.filter((todo) => {  
            if(searchText === ''){
                return todo;
            } else {
                let searchTxtToLowCase = searchText.toLowerCase();
                let todoText = todo.text.toLowerCase();
                let isInArray = todoText.indexOf(searchTxtToLowCase);

                if(isInArray >= 0){
                    return todo;
                }
            }
        });

        // sort by non completed firts
        filteredToDos.sort((a, b) => {
            if(!a.checked && b.checked){
                return -1;
            } else if(a.checked && !b.checked){
                return 1;
            } else{
                return 0;
            }
        });

        return filteredToDos;
    }
}