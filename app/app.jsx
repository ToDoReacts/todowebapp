// requirements
const React = require('react'),
      ReactDOM = require('react-dom'),
      {hashHistory} = require('react-router'),
      {Provider} = require('react-redux');


//my components
let store = require('store').configure();
const actions = require('actions');
import firebase from 'app/firebase/';
import router from 'app/router';

// load foundation
//$(document).foundation();

//load styles file
require('style!css!sass!style');

//firebase login log out controler
firebase.auth().onAuthStateChanged((user) => {
    if(user){
        store.dispatch(actions.login(user.uid,user.displayName, user.photoURL));
        store.dispatch(actions.startAddToDosInView());
        hashHistory.push('/todos');
    } else{
        store.dispatch(actions.logout());
        hashHistory.push('/');
    }
});

ReactDOM.render(
    <Provider store={store}>
        {router}
    </Provider>,
    document.getElementById('app')
);
